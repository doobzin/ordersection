﻿using Newtonsoft.Json;
using OrderSection.Models.Core.Domain;
using OrderSection.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAPI.Models.Core.Domain;
using WebAPI.ViewModel;

namespace WebAPI.Models.Core.Factries
{
    public class OrderFactoryModel
    {
        private Status Status(int id)
        {
            StatusRepository statusRepository = new StatusRepository();
            return statusRepository.GetAllStatus(id).SingleOrDefault();
        }

        private Category Category(int id)
        {
            CategoryRepository categoryRepository = new CategoryRepository();

            return categoryRepository.GetAllCategories(id).SingleOrDefault();

        }

        private Product Product(int id)
        {
            ProductRepository productRepository = new ProductRepository();

            return productRepository.GetAllProducts(id).SingleOrDefault();

        }



        public APIOrderModel Create(Order order)
        {
            decimal Total_Cost = order.Order_ItemsCollection.Where(x => x.Order_Id == order.Order_Id).Sum(op => Create(op).Row_Total);
            double Tax = Convert.ToDouble(order.Order_ItemsCollection.Where(x => x.Order_Id == order.Order_Id).Sum(op => Create(op).Row_Total)) * 0.15;

            return new APIOrderModel()
            {
                Order_Id = order.Order_Id,
                Order_Date = order.Order_Date,
                StatusName = order.Status_Name, //Status(Convert.ToInt32(order.Status_Name)).Status_Name,
                Description = order.Description, 

                Tax = Tax,
                Total_Cost = Convert.ToDecimal(Convert.ToDouble(Total_Cost) + Tax),

                Order_ItemsCollection = order.Order_ItemsCollection.Select(i => Create(i)).ToList()

            };

        }

        public APIOrder_ItemsModel Create(Order_Items order_Items)
        {
            return new APIOrder_ItemsModel()
            {
                Order_Item_Id = order_Items.Order_Item_Id,
                Order_Id = Convert.ToInt32(order_Items.Order_Id),
                Quantity = order_Items.Quantity,
                Row_Total = Product(Convert.ToInt32(order_Items.Item)).Price * order_Items.Quantity,
                Item = order_Items.Item, //Product(Convert.ToInt32(order_Items.Item)).Item , //order_Items.ProductCollection.Where(x => x.Product_Id == order_Items.Product_Id).Select(p=> Create(p)).SingleOrDefault().Item,
                Category = Category(Convert.ToInt32(order_Items.Item)).Item_Category,
               
            };
        }
        
        public APIProductModel Create(Product product)
        {
            return new APIProductModel()
            {
                Product_Id = product.Product_Id,
                Item = product.Item,
                Price = product.Price,
                Category_Id = Convert.ToInt32(product.Category_Id), 
                Category = Category(Convert.ToInt32(product.Category_Id)).Item_Category
            };
        }

        public APICategoryModel Create(Category category)
        {
            return new APICategoryModel()
            {
                Category_Id = category.Category_Id,
                Item_Category = category.Item_Category,

                ProductCollection = category.ProductCollection.Select(c => Create(c)).ToList()
            };
        }
    }
}