﻿using OrderSection.Models.Core.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebAPI.Models.Core.Domain
{
    public class APIProductModel
    {

        [Key]
        [DisplayName("Item Id")]
        public int Product_Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Item { get; set; }
        
        public decimal Price { get; set; }
        
        public int Category_Id { get; set; }

        public string Category { get; set; }

    }
}