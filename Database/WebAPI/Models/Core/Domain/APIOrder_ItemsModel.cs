﻿using OrderSection.Models.Core.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebAPI.Models.Core.Domain
{
    public class APIOrder_ItemsModel
    {
        [Key]
        public int Order_Item_Id { get; set; }

        public int Quantity { get; set; }

        [Column(TypeName = "money")]
        public decimal Row_Total { get; set; }
        
        private int Product_Id { get; set; }

        public string Item { get; set; }

        public string Category { get; set; }

        public int Order_Id { get; set; }

        public string Description { get; set; }

    }
}