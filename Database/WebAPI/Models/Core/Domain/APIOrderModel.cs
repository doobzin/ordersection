﻿using Newtonsoft.Json;
using OrderSection.Models.Core.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebAPI.Models.Core.Domain
{
    public class APIOrderModel
    {
        [Key]
        [DisplayName("Order Number")]
        public int Order_Id { get; set; }

        public DateTime Order_Date { get; set; }

        public decimal Total_Cost { get; set; }

        public string Description { get; set; }
        
        public double Tax { get; set; }

        public string StatusName { get; set; }

        public virtual ICollection<APIOrder_ItemsModel> Order_ItemsCollection { get; set; }
    }
}