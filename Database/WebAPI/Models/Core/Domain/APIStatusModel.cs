﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAPI.Models.Core.Domain
{
    public class APIStatusModel
    {
        [Key]
        public int Status_Id { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Status")]
        public string Status_Name { get; set; }

        public virtual ICollection<APIOrderModel> OrderCollection { get; set; }
    }
}