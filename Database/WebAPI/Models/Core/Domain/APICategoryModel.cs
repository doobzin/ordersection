﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAPI.Models.Core.Domain
{
    public class APICategoryModel
    {
        [Key]
        public int Category_Id { get; set; }

        [Required]
        [StringLength(10)]
        public string Item_Category { get; set; }

        public virtual IEnumerable<APIProductModel> ProductCollection { get; set; }
    }
}