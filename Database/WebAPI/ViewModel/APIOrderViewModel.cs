﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.ViewModel
{
    

    public partial class APIOrderViewModel
    {
        public int Order_Number { get; set; }

        public DateTime Order_Date { get; set; }

        public decimal Total_Cost { get; set; }
        
        public int Status { get; set; }

        public string Description { get; set; }

        public string Item { get; set; }

        public string Item_Category { get; set; }

        public int Quantity { get; set; }
        

    }

    

}