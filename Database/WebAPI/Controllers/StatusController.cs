﻿using OrderSection.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebAPI.Controllers
{
    public class StatusController : ApiController
    {
        StatusRepository statusRepository = new StatusRepository();
        // GET: api/Status
        public HttpResponseMessage Get()
        {
            var entity = statusRepository.GetAllStatus().ToList()
                                     .Select(o => o);

            if (entity != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, entity);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No statuses as yet");
            }
        }

        // GET: api/Status/5
        public HttpResponseMessage Get(int id)
        {
            var entity = statusRepository.GetAllStatus().ToList()
                                        .Where(o => o.Status_Id == id)
                                        .Select(o => o);

            if (entity != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, entity);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No statuses as yet");
            }
        }

        // POST: api/Status
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Status/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Status/5
        public void Delete(int id)
        {
        }
    }
}
