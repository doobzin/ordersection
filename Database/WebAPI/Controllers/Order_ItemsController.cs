﻿using orderSection.Repo.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebAPI.Controllers
{
    public class Order_ItemsController : ApiController
    {
        Order_ItemsRepository order_ItemsRepository = new Order_ItemsRepository();
        // GET: api/Order_Items
        public HttpResponseMessage Get()
        {
            var entity = order_ItemsRepository.GetAllOrders()
                                    .Select(o => o);

            if (entity != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, entity);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No order items as yet");
            }
        }

        // GET: api/Order_Items/5
        public HttpResponseMessage Get(int id)
        {
            var entity = order_ItemsRepository.GetAllOrders().ToList()
                                        .Where(oi => oi.Order_Item_Id == id)
                                       .Select(oi => oi);

            if (entity != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, entity);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No order items as yet");
            }
        }

        // POST: api/Order_Items
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Order_Items/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Order_Items/5
        public void Delete(int id)
        {
        }
    }
}
