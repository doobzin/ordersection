﻿using OrderSection.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Models.Core.Domain;

namespace WebAPI.Controllers
{
    public class ProductController : ApiController
    {
        ProductRepository productRepository = new ProductRepository();
        // GET: api/Product
        public HttpResponseMessage Get()
        {
            var entity = productRepository.GetAllProducts().ToList()
                                     .Select(o => o);

            if (entity != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, entity);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No products as yet");
            }
        }

        // GET: api/Product/5
        public HttpResponseMessage Get(int id)
        {
            var entity = productRepository.GetAllProducts().ToList()
                                  .Where(o => o.Product_Id == id)
                                  .Select(x => x);

            if (entity != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, entity);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Product with ID = " + id.ToString() + " not found");
            }

        }

        // POST: api/Product
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Product/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Product/5
        public void Delete(int id)
        {
        }
    }
}
