﻿using OrderSection.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebAPI.Controllers
{
    public class CategoryController : ApiController
    {
        CategoryRepository categoryRepository = new CategoryRepository();

        // GET: api/Category
        public HttpResponseMessage Get()
        {
            var entity = categoryRepository.GetAllCategories().ToList()
                                    .Select(o => o);

            if (entity != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, entity);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No categories as yet");
            }
        }

        // GET: api/Category/5
        public HttpResponseMessage Get(int id)
        {
            var entity = categoryRepository.GetAllCategories().ToList()
                                    .Where(c => c.Category_Id == id)
                                    .Select(o => o);

            if (entity != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, entity);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No categories as yet");
            }
        }

        // POST: api/Category
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Category/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Category/5
        public void Delete(int id)
        {
        }
    }
}
