﻿using OrderSection.Models.Core.Domain;
using OrderSection.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Models.Core.Domain;
using WebAPI.Models.Core.Factries;

namespace WebAPI.Controllers
{
    public class OrdersController : ApiController
    {
        OrderFactoryModel OrderFactory;

        OrderRepository orderRepository = new OrderRepository();
        public OrdersController()
        {
            OrderFactory = new OrderFactoryModel();
        }

        // GET api/values
        public HttpResponseMessage Get()
        {

            var entity = orderRepository.GetAllOrders().ToList();
                                  //.Select(o=> OrderFactory.Create(o));

            if (entity != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, entity);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No order as yet");
            }

        }

        // GET api/values/5
        public HttpResponseMessage Get(int id)
        {
            var entity = orderRepository.GetAllOrders().ToList()
                                  .Where(o => o.Order_Id == id)
                                  .ToList();//.Select(x => OrderFactory.Create(x));

            if (entity != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, entity);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Order with ID = "+ id.ToString()+" not found");
            }

        }

        // POST api/values
        public HttpResponseMessage Post(Order order)
        {
            try
            {
                orderRepository.Post(order);

                var message = Request.CreateResponse(HttpStatusCode.Created, order);
                message.Headers.Location = new Uri(Request.RequestUri + order.Order_Id.ToString());
                return message;
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
            }
        }

        // PUT api/values/5
        public HttpResponseMessage Put(int id, Order order)
        {
            try
            {
                orderRepository.Put(id, order);
                return Request.CreateResponse(HttpStatusCode.OK, order);
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Order with ID: " + id.ToString() + " was not found to update");
            }
        }

        // DELETE api/values/5
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                orderRepository.Delete(id);
               return Request.CreateResponse(HttpStatusCode.OK); 
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Order with ID: " + id.ToString() + " Not found to delete");
            }
        }
    }
}
