namespace OrderSection.Models.Core.Domain
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Product")]
    public partial class Product
    {

        [Key]
        [DisplayName("Item Id")]
        public int Product_Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Item { get; set; }

        [Column(TypeName = "money")]
        public decimal Price { get; set; }

        //[ForeignKey("Order_Items")]
        //public Nullable<int> Order_Items_Id { get; set; }
        //public Order_Items Order_Items { get; set; }

        [ForeignKey("Category")]//try nullable and enum
        public Nullable<int> Category_Id { get; set; }
        public virtual Category Category { get; set; }
    }
}
