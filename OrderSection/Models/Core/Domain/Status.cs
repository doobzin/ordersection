namespace OrderSection.Models.Core.Domain
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Status
    {
        [Key]
        public int Status_Id { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Status")]
        public string Status_Name { get; set; }

        public virtual ICollection<Order> OrderCollection { get; set; }
    }
}
