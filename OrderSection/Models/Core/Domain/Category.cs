namespace OrderSection.Models.Core.Domain
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Category")]
    public partial class Category
    {

        public Category()
        {
            this.ProductCollection = new HashSet<Product>();
        }

        [Key]
        public int Category_Id { get; set; }

        [Required]
        [StringLength(10)]
        public string Item_Category { get; set; }
        
        public virtual ICollection<Product> ProductCollection { get; set; }
    }
}
