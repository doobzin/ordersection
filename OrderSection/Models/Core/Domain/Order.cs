namespace OrderSection.Models.Core.Domain
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Order")]
    public partial class Order
    {
        public Order()
        {
            this.Order_ItemsCollection = new HashSet<Order_Items>();
        }

        [Key]
        [DisplayName("Order Number")]
        public int Order_Id { get; set; }

        public DateTime Order_Date { get; set; }


        [Required]
        [StringLength(50, ErrorMessage = "Discription lenght is more then 50 charectors")]
        public string Description { get; set; }


        public decimal Total_Cost { get; set; }

        public string Status_Name { get; set; }
        //[ForeignKey("Status")]
        //public Nullable<int> Status_Id { get; set; }

        //public virtual Status Status { get; set; }
        
        public virtual ICollection<Order_Items> Order_ItemsCollection { get; set; }
    }
}
