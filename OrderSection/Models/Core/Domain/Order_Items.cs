namespace OrderSection.Models.Core.Domain
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Order_Items
    {

        [Key]
        public int Order_Item_Id { get; set; }

        public int Quantity { get; set; }

        [Column(TypeName = "money")]
        public decimal Row_Total { get; set; }

        //[ForeignKey("ProductCollection")]
        //public Nullable<int> Product_Id { get; set; }
        //public virtual ICollection<Product> ProductCollection { get; set; }

        public string Item { get; set; }

        [ForeignKey("Order")]
        public Nullable<int> Order_Id { get; set; }
        public virtual Order Order { get; set; }
    }
}
