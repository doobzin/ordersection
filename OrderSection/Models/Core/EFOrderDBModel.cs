namespace OrderSection.Models.Core.Domain
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class EFOrderDBModel : DbContext
    {
        public EFOrderDBModel()
            : base("name=EFOrderDBModel")
        {
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
        }

        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<Order_Items> Order_Items { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Status> Status { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Order_Items>()
            //    .HasRequired<Order>(s => s.Order)
            //    .WithMany(s => s.Order_ItemsCollection);

            //modelBuilder.Entity<Product>()
            //    .HasRequired<Order_Items>(s => s.Order_Items)
            //    .WithMany(s => s.ProductCollection);

            //modelBuilder.Entity<Product>()
            //    .HasRequired<Category>(s => s.Category)
            //    .WithMany(s => s.ProductCollection);

            //modelBuilder.Entity<Order>()
            //    .HasRequired<Status>(s => s.Status)
            //    .WithMany(s => s.OrderCollection);

            //modelBuilder.Entity<Category>()
            //    .Property(e => e.Item_Category)
            //    .IsFixedLength();

            //modelBuilder.Entity<Order>()
            //    .Property(e => e.Total_Cost)
            //    .HasPrecision(18, 0);

            modelBuilder.Entity<Order>();
                //.Property(e => e.Status_Id);
            
            modelBuilder.Entity<Order_Items>()
                .Property(e => e.Row_Total)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Product>()
                .Property(e => e.Price)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Status>()
                .Property(e => e.Status_Name)
                .IsUnicode(false);
        }
    }
}
