namespace OrderSection.Migrations
{
    using OrderSection.Models.Core.Domain;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<OrderSection.Models.Core.Domain.EFOrderDBModel>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(OrderSection.Models.Core.Domain.EFOrderDBModel context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            List<Category> categories = new List<Category>()
            {
                new Category{ Category_Id = 1, Item_Category = "Laptop" },
                new Category{ Category_Id = 2, Item_Category = "Desktop" },
                new Category{ Category_Id = 3, Item_Category = "Motherboard" }
            };
            categories.ForEach(c => context.Categories.AddOrUpdate(x => x.Category_Id, c));
            context.SaveChanges();

            List<Status> statuses = new List<Status>()
            {
                new Status{ Status_Id = 1, Status_Name = "Padding" },
                new Status{ Status_Id = 1, Status_Name = "Accepted" },
                new Status{ Status_Id = 1, Status_Name = "Completed" },
            };
            statuses.ForEach(s => context.Status.AddOrUpdate(x => x.Status_Id, s));
            context.SaveChanges();

            List<Product> products = new List<Product>() {
                new Product{ Product_Id = 1, Item = "Mac book", Category_Id = 1, Price = 24000 },
                new Product{ Product_Id = 2, Item = "Lenovo", Category_Id = 1, Price = 12000 },
                new Product{ Product_Id = 3, Item = "Sahara", Category_Id = 2, Price = 14000 }
            };
            products.ForEach(p => context.Products.AddOrUpdate(x => x.Product_Id, p));
            context.SaveChanges();

            List<Order> orders = new List<Order>() {
                new Order { Order_Id = 1, Order_Date = DateTime.Now.AddDays(-4), Status_Name = "Pannding" },
                new Order { Order_Id = 2, Order_Date = DateTime.Now.AddDays(-2), Status_Name = "Pannding" },
                new Order { Order_Id = 3, Order_Date = DateTime.Now, Status_Name = "Completed" }
            };
            orders.ForEach(o => context.Orders.AddOrUpdate(x => x.Order_Id, o));
            context.SaveChanges();

            List<Order_Items> order_items = new List<Order_Items>() {
                new Order_Items{ Order_Item_Id = 1, Quantity = 2, Order_Id = 1 },
                new Order_Items{ Order_Item_Id = 2, Quantity = 4, Order_Id = 1 },
                new Order_Items{ Order_Item_Id = 3, Quantity = 1, Order_Id = 1 }
            };
            order_items.ForEach(o => context.Order_Items.AddOrUpdate(x => x.Order_Id, o));
            context.SaveChanges();
        }
    }
}
