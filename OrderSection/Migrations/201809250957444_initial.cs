namespace OrderSection.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Category",
                c => new
                    {
                        Category_Id = c.Int(nullable: false, identity: true),
                        Item_Category = c.String(nullable: false, maxLength: 10),
                    })
                .PrimaryKey(t => t.Category_Id);
            
            CreateTable(
                "dbo.Product",
                c => new
                    {
                        Product_Id = c.Int(nullable: false, identity: true),
                        Item = c.String(nullable: false, maxLength: 50),
                        Price = c.Decimal(nullable: false, storeType: "money"),
                        Category_Id = c.Int(nullable: false),
                        Order_Items_Order_Item_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Product_Id)
                .ForeignKey("dbo.Category", t => t.Category_Id, cascadeDelete: true)
                .ForeignKey("dbo.Order_Items", t => t.Order_Items_Order_Item_Id)
                .Index(t => t.Category_Id)
                .Index(t => t.Order_Items_Order_Item_Id);
            
            CreateTable(
                "dbo.Order_Items",
                c => new
                    {
                        Order_Item_Id = c.Int(nullable: false, identity: true),
                        Quantity = c.Int(nullable: false),
                        Row_Total = c.Decimal(nullable: false, storeType: "money"),
                        Product_Id = c.Int(nullable: false),
                        Order_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Order_Item_Id)
                .ForeignKey("dbo.Order", t => t.Order_Id, cascadeDelete: true)
                .Index(t => t.Order_Id);
            
            CreateTable(
                "dbo.Order",
                c => new
                    {
                        Order_Id = c.Int(nullable: false, identity: true),
                        Order_Date = c.DateTime(nullable: false),
                        Total_Cost = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Status_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Order_Id)
                .ForeignKey("dbo.Status", t => t.Status_Id, cascadeDelete: true)
                .Index(t => t.Status_Id);
            
            CreateTable(
                "dbo.Status",
                c => new
                    {
                        Status_Id = c.Int(nullable: false, identity: true),
                        Status_Name = c.String(nullable: false, maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.Status_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Product", "Order_Items_Order_Item_Id", "dbo.Order_Items");
            DropForeignKey("dbo.Order_Items", "Order_Id", "dbo.Order");
            DropForeignKey("dbo.Order", "Status_Id", "dbo.Status");
            DropForeignKey("dbo.Product", "Category_Id", "dbo.Category");
            DropIndex("dbo.Order", new[] { "Status_Id" });
            DropIndex("dbo.Order_Items", new[] { "Order_Id" });
            DropIndex("dbo.Product", new[] { "Order_Items_Order_Item_Id" });
            DropIndex("dbo.Product", new[] { "Category_Id" });
            DropTable("dbo.Status");
            DropTable("dbo.Order");
            DropTable("dbo.Order_Items");
            DropTable("dbo.Product");
            DropTable("dbo.Category");
        }
    }
}
