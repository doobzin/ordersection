namespace OrderSection.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class moveDiscriptionToOrder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Order", "Description", c => c.String(nullable: false, maxLength: 50));
            DropColumn("dbo.Product", "Description");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Product", "Description", c => c.String(nullable: false, maxLength: 50));
            DropColumn("dbo.Order", "Description");
        }
    }
}
