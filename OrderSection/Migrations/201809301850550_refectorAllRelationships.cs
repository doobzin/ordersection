namespace OrderSection.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class refectorAllRelationships : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Product", "Order_Items_Id", "dbo.Order_Items");
            DropIndex("dbo.Product", new[] { "Order_Items_Id" });
            RenameColumn(table: "dbo.Order", name: "Status_Id", newName: "Status_Status_Id");
            RenameIndex(table: "dbo.Order", name: "IX_Status_Id", newName: "IX_Status_Status_Id");
            AddColumn("dbo.Order_Items", "Item", c => c.String());
            AddColumn("dbo.Order", "Status_Name", c => c.String());
            DropColumn("dbo.Product", "Order_Items_Id");
            DropColumn("dbo.Order_Items", "Product_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Order_Items", "Product_Id", c => c.Int());
            AddColumn("dbo.Product", "Order_Items_Id", c => c.Int());
            DropColumn("dbo.Order", "Status_Name");
            DropColumn("dbo.Order_Items", "Item");
            RenameIndex(table: "dbo.Order", name: "IX_Status_Status_Id", newName: "IX_Status_Id");
            RenameColumn(table: "dbo.Order", name: "Status_Status_Id", newName: "Status_Id");
            CreateIndex("dbo.Product", "Order_Items_Id");
            AddForeignKey("dbo.Product", "Order_Items_Id", "dbo.Order_Items", "Order_Item_Id");
        }
    }
}
