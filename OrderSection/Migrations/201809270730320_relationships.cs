namespace OrderSection.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class relationships : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Order", "Status_Id", "dbo.Status");
            DropIndex("dbo.Order", new[] { "Status_Id" });
            AlterColumn("dbo.Order_Items", "Product_Id", c => c.Int());
            AlterColumn("dbo.Order_Items", "Order_Id", c => c.Int());
            AlterColumn("dbo.Order", "Status_Id", c => c.Int());
            AlterColumn("dbo.Product", "Order_Items_Id", c => c.Int());
            AlterColumn("dbo.Product", "Category_Id", c => c.Int());
            CreateIndex("dbo.Product", "Order_Items_Id");
            CreateIndex("dbo.Product", "Category_Id");
            CreateIndex("dbo.Order_Items", "Order_Id");
            CreateIndex("dbo.Order", "Status_Id");
            AddForeignKey("dbo.Product", "Category_Id", "dbo.Category", "Category_Id");
            AddForeignKey("dbo.Order_Items", "Order_Id", "dbo.Order", "Order_Id");
            AddForeignKey("dbo.Product", "Order_Items_Id", "dbo.Order_Items", "Order_Item_Id");
            AddForeignKey("dbo.Order", "Status_Id", "dbo.Status", "Status_Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Order", "Status_Id", "dbo.Status");
            DropForeignKey("dbo.Product", "Order_Items_Id", "dbo.Order_Items");
            DropForeignKey("dbo.Order_Items", "Order_Id", "dbo.Order");
            DropForeignKey("dbo.Product", "Category_Id", "dbo.Category");
            DropIndex("dbo.Order", new[] { "Status_Id" });
            DropIndex("dbo.Order_Items", new[] { "Order_Id" });
            DropIndex("dbo.Product", new[] { "Category_Id" });
            DropIndex("dbo.Product", new[] { "Order_Items_Id" });
            AlterColumn("dbo.Product", "Category_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Product", "Order_Items_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Order", "Status_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Order_Items", "Order_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Order_Items", "Product_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.Order", "Status_Id");
            AddForeignKey("dbo.Order", "Status_Id", "dbo.Status", "Status_Id", cascadeDelete: true);
        }
    }
}
