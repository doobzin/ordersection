﻿using OrderSection.Models.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderSection.Models.Repository
{
    public class CategoryRepository
    {
        EFOrderDBModel context = new EFOrderDBModel();

        public IEnumerable<Category> GetAllCategories()
        {
            return context.Categories.Select(c => c);
        }

        public IEnumerable<Category> GetAllCategories(int id)
        {
            return context.Categories.Where(c => c.Category_Id == id)
                                     .Select(x => x);
        }
    }
}
