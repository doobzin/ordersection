﻿using OrderSection.Models.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace orderSection.Repo.Repository
{
    public class Order_ItemsRepository
    {
        EFOrderDBModel context = new EFOrderDBModel();

        public IEnumerable<Order_Items> GetAllOrders()
        {
            return context.Order_Items.Select(x => x);
        }

        public IEnumerable<Order_Items> GetAllOrders(int id)
        {
            return context.Order_Items.Where(o => o.Order_Item_Id == id)
                                 .Select(x => x);
        }

        public void Post(Order_Items order_item)
        {
            context.Order_Items.Add(order_item);
            context.SaveChanges();
        }

        public void Put(int id, Order_Items order_item)
        {

            var entity = context.Order_Items.FirstOrDefault(o => o.Order_Id == id);

            entity.Order_Id = order_item.Order_Id;
            entity.Item = order_item.Item;
            entity.Quantity = order_item.Quantity;
            entity.Row_Total = order_item.Row_Total;

            context.SaveChanges();
        }

        public Order_Items Delete(int id)
        {
            var entity = context.Order_Items.FirstOrDefault(o => o.Order_Item_Id == id);

            context.SaveChanges();
            return entity;
        }
    }
}
