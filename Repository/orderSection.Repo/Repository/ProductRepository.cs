﻿using OrderSection.Models.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderSection.Models.Repository
{
    public class ProductRepository
    {
        EFOrderDBModel context = new EFOrderDBModel();

        public IEnumerable<Product> GetAllProducts()
        {
            return context.Products.Select(c => c);
        }

        public IEnumerable<Product> GetAllProducts(int id)
        {
            return context.Products.Where(c => c.Product_Id == id)
                                     .Select(x => x);
        }
    }
}
