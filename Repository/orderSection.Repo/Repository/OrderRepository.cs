﻿using OrderSection.Models.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity;

namespace OrderSection.Models.Repository
{
    public class OrderRepository
    {
        EFOrderDBModel context = new EFOrderDBModel();

        public IEnumerable<Order> GetAllOrders()
        {
            return context.Orders.Include(o => o.Order_ItemsCollection).Select(x=>x);
                                                //.Select(p => p.ProductCollection
                                                //.Select(c => c.Category)))
                                 //.Select(x=>x));
        }

        public IEnumerable<Order> GetAllOrders(int id)
        {
            return context.Orders.Where(o => o.Order_Id == id)
                                 .Include(oi => oi.Order_ItemsCollection)
                                 .Select(x => x);
        }

        public Order Post(Order order) {
            
            context.Orders.Add(order);
            context.SaveChanges();
            return order;
        }

        public Order Put(int id, Order order)
        {

            var entity = context.Orders.FirstOrDefault(o => o.Order_Id == id);

            entity.Order_Date = order.Order_Date;
            entity.Total_Cost = order.Total_Cost;
            entity.Status_Name = order.Status_Name;

            context.SaveChanges();

            return order;
        }

        public Order Delete(int id)
        {
            var entity =  context.Orders.FirstOrDefault(o=>o.Order_Id == id);

            context.SaveChanges();
            return entity;
        }
    }
}
