﻿using OrderSection.Models.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderSection.Models.Repository
{
    public class StatusRepository
    {
        EFOrderDBModel context = new EFOrderDBModel();

        public IEnumerable<Status> GetAllStatus()
        {
            return context.Status.Select(c => c);
        }

        public IEnumerable<Status> GetAllStatus(int id)
        {
            return context.Status.Where(c => c.Status_Id == id)
                                     .Select(x => x);
        }
    }
}
