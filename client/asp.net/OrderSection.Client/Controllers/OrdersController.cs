﻿using OrderSection.Client.Controllers.Helpers;
using OrderSection.Client.Models;
using OrderSection.Client.Models.Factory;
using OrderSection.Client.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace OrderSection.Client.Controllers
{
    public class OrdersController : Controller
    {
        OrderFactoryModel OrderFactory;
        
        public OrdersController()
        {
            OrderFactory = new OrderFactoryModel();
        }

        // GET: Orders 
        public ActionResult Index()
        {

            var order = GetOrdersFromAPI().Select(o => o);
            var product = GetProductFromAPI().Select(p => p);
            var category = GetCategoryFromAPI().Select(c => c);
            var status = GetStatusFromAPI().Select(s => s);
            var order_items = GetOrder_ItemsFromAPI().Select(oi => oi);

            var ModelIndexer = new OrderModelIdexer();

            ModelIndexer.ProductEntityCollection = product;
            ModelIndexer.OrderEntityCollection = order;
            ModelIndexer.CategoryEntityCollection = category;
            ModelIndexer.StatusEntityCollection = status;
            ModelIndexer.Order_ItemsEntityCollection = order_items;

            return View(ModelIndexer);
        }

        private IEnumerable<Order_ItemsEntity> GetOrder_ItemsFromAPI()
        {
            return ControllerHelper.Get<Order_ItemsEntity>("Order_Items");
        }

        private IEnumerable<StatusEntity> GetStatusFromAPI()
        {
            return ControllerHelper.Get<StatusEntity>("Status");
        }

        private IEnumerable<CategoryEntity> GetCategoryFromAPI()
        {
            return ControllerHelper.Get<CategoryEntity>("Category");
        }

        private IEnumerable<ProductEntity> GetProductFromAPI()
        {
            return ControllerHelper.Get<ProductEntity>("Product");
        }

        private IEnumerable<OrderEntity> GetOrdersFromAPI()
        {
            return ControllerHelper.Get<OrderEntity>("Orders");
        }
        
        public ActionResult Create(OrderEntity order)
        {
            if (ModelState.IsValid)
            {
                var isCreated = ControllerHelper.isCreated<OrderEntity>("Orders", order);

                if(isCreated == true)
                {
                    ViewBag.Result = "Data was succefully saved";
                    ModelState.Clear();
                    return View(new OrderEntity());
                }
                else
                {
                    ViewBag.Result = "Error: Invalid data.";
                }
            }
            return View(order);

        }

        public ActionResult Edit(OrderEntity order)
        {
            if (ModelState.IsValid)
            {
                var isEdited = ControllerHelper.isEdited<OrderEntity>("Orders", order);

                if (isEdited == true)
                {
                    ModelState.Clear();
                    return View(new OrderEntity());
                }
                else
                {
                    ViewBag.Result = "Error: Invalid data.";
                }
            }
            return View(order);

        }
    }
    
}