﻿using OrderSection.Client.Controllers.Helpers;
using OrderSection.Client.Models;
using OrderSection.Client.Models.Factory;
using OrderSection.Client.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OrderSection.Client.Controllers
{
    public class EddiController : Controller
    {
        OrderFactoryModel OrderFactory;
        public EddiController()
        {
            OrderFactory = new OrderFactoryModel(); 
        }
        // GET: Eddi
        public ActionResult Index()
        {
            var OrdersFromAPI = ControllerHelper.Get<OrderEntity>("Orders");
            var order = OrdersFromAPI.Select(o => o);

            return View(order);
        }
        // GET: Eddi/Details/5
        public ActionResult Details(int id)
        {
            var OrdersFromAPI = ControllerHelper.Get<OrderEntity>("Orders");
            var order = OrdersFromAPI.Where(o => o.Order_Id == id).SingleOrDefault();

            return View(order);
        }

        // GET: Eddi/Create
        public ActionResult Create()
        {
            OrderModelIdexer orderModelIdexer = new OrderModelIdexer();
            
            orderModelIdexer.CategoryEntityCollection = ControllerHelper.Get<CategoryEntity>("Category");
            orderModelIdexer.StatusEntityCollection = ControllerHelper.Get<StatusEntity>("Status");
            orderModelIdexer.ProductEntityCollection = ControllerHelper.Get<ProductEntity>("Product");
            orderModelIdexer.OrderEntityCollection = ControllerHelper.Get<OrderEntity>("Order");
            orderModelIdexer.Order_ItemsEntityCollection = ControllerHelper.Get<Order_ItemsEntity>("Order_Items");

            orderModelIdexer.OrderEntity = ControllerHelper.Get<OrderEntity>("Orders").Where(x => x.Order_Id == 2).SingleOrDefault(); //.Select(o => OrderFactory.Create(o)).SingleOrDefault(); ///.Where(x => x.Order_Id == 2).SingleOrDefault();
            return View(orderModelIdexer);
        }

        // POST: Eddi/Create
        [HttpPost]
        public ActionResult Create(OrderEntity collection)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    var isCreated = ControllerHelper.isCreated<OrderEntity>("Orders", collection);

                    if (isCreated == true)
                    {
                        ViewBag.Result = "Data was succefully saved";
                        ModelState.Clear();
                        return View(new OrderEntity());
                    }
                    else
                    {
                        ViewBag.Result = "Error: Invalid data.";
                    }
                }
                return View(collection);

                //ControllerHelper.isCreated("Orders", collection);
                //return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Eddi/Edit/5
        public ActionResult Edit(int id)
        {
            var OrdersFromAPI = ControllerHelper.Get<OrderEntity>("Orders");
            var order = OrdersFromAPI.Where(o => o.Order_Id == id).SingleOrDefault();

            return View(order);
        }

        // POST: Eddi/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Eddi/Delete/5
        public ActionResult Delete(int id)
        {
            var OrdersFromAPI = ControllerHelper.Get<OrderEntity>("Orders");
            var order = OrdersFromAPI.Where(o => o.Order_Id == id).SingleOrDefault();

            return View(order);
        }

        // POST: Eddi/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
