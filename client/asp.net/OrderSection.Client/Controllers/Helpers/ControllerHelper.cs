﻿using OrderSection.Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace OrderSection.Client.Controllers.Helpers
{
    public class ControllerHelper
    {
        private static string baseUrl = "http://localhost:1668/Api/";

        //try to make it one for all
        public static List<T> Get<T>(string entity)
        {
            try
            {
                var resultList = new List<T>();
                var client = new HttpClient();
                var GetDataTask = client.GetAsync(baseUrl + entity).ContinueWith(response =>
                {
                    var result = response.Result;
                    if (result.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var readResult = result.Content.ReadAsAsync<List<T>>();
                        readResult.Wait();

                        resultList = readResult.Result.Select(x => x).ToList();
                    }
                });

                GetDataTask.Wait();
                return resultList;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static bool isCreated<T>(string path, T entity)
        {
            //var client = new HttpClient();
            //var result = client.PutAsJsonAsync(baseUrl+path, entity).Result;
            //if (result.IsSuccessStatusCode)
            //{
            //    entity = result.Content.ReadAsAsync<T>().Result;

            //    List<T> orders = new List<T>();
            //    HttpClient client2 = new HttpClient();
            //    var result2 = client2.GetAsync(baseUrl+entity).Result;

            //    if (result2.IsSuccessStatusCode)
            //    {
            //        orders = result2.Content.ReadAsAsync<List<T>>().Result;
            //    }
            //   return true;
            //}
            //else
            //{
            //    return false;
            //}
            try
            {
                var resultList = new List<T>();
                var client = new HttpClient();
                var GetDataTask = client.PostAsJsonAsync(baseUrl + entity, entity).ContinueWith(response =>
                {
                    var result = response.Result;
                    if (result.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var readResult = result.Content.ReadAsAsync<List<T>>();
                        readResult.Wait();

                        resultList = readResult.Result.Select(x => x).ToList();
                    }
                });

                GetDataTask.Wait();
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }

        }


        public static bool isEdited<T>(string path, T entity)
        {
            //    var client = new HttpClient();
            //    var result = client.PutAsJsonAsync(baseUrl+path, entity).Result;
            //    if (result.IsSuccessStatusCode)
            //    {
            //        entity = result.Content.ReadAsAsync<T>().Result;

            //        List<T> orders = new List<T>();
            //        HttpClient client2 = new HttpClient();
            //        var result2 = client2.GetAsync(baseUrl+path).Result;

            //        if (result2.IsSuccessStatusCode)
            //        {
            //            orders = result2.Content.ReadAsAsync<List<T>>().Result;
            //        }
            //        return true;

            //}
            //else
            //    {
            //        return false;
            //    }
            //}

            try
            {
                var resultList = new List<T>();
                var client = new HttpClient();
                var GetDataTask = client.GetAsync(baseUrl + entity).ContinueWith(response =>
                {
                    var result = response.Result;
                    if (result.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var readResult = result.Content.ReadAsAsync<List<T>>();
                        readResult.Wait();

                        resultList = readResult.Result.Select(x => x).ToList();
                    }
                });

                GetDataTask.Wait();
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}