﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OrderSection.Client.Models
{
    public class OrderEntity
    {
        [Key]
        [DisplayName("Order Number")]
        public int Order_Id { get; set; }

        public DateTime Order_Date { get; set; }

        public string Description { get; set; }

        public decimal Total_Cost { get; set; }

        public double Tax { get; set; }

        public string StatusName { get; set; }

        public virtual ICollection<Order_ItemsEntity> Order_ItemsCollection { get; set; }
    }
}