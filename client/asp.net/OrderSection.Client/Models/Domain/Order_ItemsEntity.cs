﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OrderSection.Client.Models
{
    public class Order_ItemsEntity
    {
        public int Order_Item_Id { get; set; }

        public int Order_Id { get; set; }

        public int Quantity { get; set; }
        
        public int Product_Id { get; set; }

        public string Item { get; set; }

        public string Category { get; set; }
        
        [Column(TypeName = "money")]
        public decimal Row_Total { get; set; }

        public virtual ICollection<ProductEntity> ProductCollection { get; set; }
    }
}