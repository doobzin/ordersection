﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OrderSection.Client.Models
{
    public class ProductEntity
    {
        [Key]
        [DisplayName("Item Id")]
        public int Product_Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Item { get; set; }

        public decimal Price { get; set; }
        
        public int Category_Id { get; set; }

        public string Category { get; set; }
    }
}