﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OrderSection.Client.Models
{
    public class StatusEntity
    {
        [Key]
        public int Status_Id { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Status")]
        public string Status_Name { get; set; }

        public virtual ICollection<OrderEntity> OrderCollection { get; set; }
    }
}