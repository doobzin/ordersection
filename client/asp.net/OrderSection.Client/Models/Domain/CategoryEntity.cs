﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OrderSection.Client.Models
{
    public class CategoryEntity
    {
        [Key]
        public int Category_Id { get; set; }

        [Required]
        [StringLength(10)]
        public string Item_Category { get; set; }

        public virtual IEnumerable<ProductEntity> ProductCollection { get; set; }
    }
}