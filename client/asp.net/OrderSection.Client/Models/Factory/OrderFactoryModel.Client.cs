﻿using Newtonsoft.Json;
using OrderSection.Client.Models;
using OrderSection.Client.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OrderSection.Client.Models.Factory
{
    public class OrderFactoryModel
    {
        OrderModelIdexer EntityModel = new OrderModelIdexer();

        private StatusEntity Status(int id)
        {
            return EntityModel.StatusEntityCollection.Where(s => s.Status_Id == id).SingleOrDefault();
        }

        private CategoryEntity Category(int id)
        {
            return EntityModel.CategoryEntityCollection.Where(c => c.Category_Id == id).SingleOrDefault();
        }

        private ProductEntity Product(int id)
        {
            return EntityModel.ProductEntityCollection.Where(p => p.Product_Id == id).SingleOrDefault();
        }



        public OrderEntity Create(OrderEntity order)
        {
            decimal Total_Cost = order.Order_ItemsCollection.Where(x => x.Order_Id == order.Order_Id).Sum(op => Create(op).Row_Total);
            double Tax = Convert.ToDouble(order.Order_ItemsCollection.Where(x => x.Order_Id == order.Order_Id).Sum(op => Create(op).Row_Total)) * 0.15;

            return new OrderEntity()
            {
                Order_Id = order.Order_Id,
                Order_Date = order.Order_Date,
                StatusName = order.StatusName, //Status(Convert.ToInt32(order.Status_Id)).Status_Name,
                Description = order.Description,

                Tax = Tax,
                Total_Cost = Convert.ToDecimal(Convert.ToDouble(Total_Cost) + Tax),

                Order_ItemsCollection = order.Order_ItemsCollection.Select(i => Create(i)).ToList()

            };

        }

        public Order_ItemsEntity Create(Order_ItemsEntity order_Items)
        {
            return new Order_ItemsEntity()
            {
                Order_Item_Id = order_Items.Order_Item_Id,
                Order_Id = Convert.ToInt32(order_Items.Order_Id),
                Quantity = order_Items.Quantity,
                Row_Total = Product(Convert.ToInt32(order_Items.Product_Id)).Price * order_Items.Quantity,
                Item = Product(Convert.ToInt32(order_Items.Product_Id)).Item ,  
                Category = Category(Convert.ToInt32(order_Items.Product_Id)).Item_Category 
            };
        }
        
        public ProductEntity Create(ProductEntity product)
        {
            return new ProductEntity()
            {
                Product_Id = product.Product_Id,
                Item = product.Item,
                Price = product.Price,
                Category_Id = Convert.ToInt32(product.Category_Id), 
                Category = Category(Convert.ToInt32(product.Category_Id)).Item_Category
            };
        }

        public CategoryEntity Create(CategoryEntity category)
        {
            return new CategoryEntity()
            {
                Category_Id = category.Category_Id,
                Item_Category = category.Item_Category,

                ProductCollection = category.ProductCollection.Select(c => Create(c)).ToList()
            };
        }
    }
}