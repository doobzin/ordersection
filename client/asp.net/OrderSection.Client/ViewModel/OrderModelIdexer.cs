﻿using OrderSection.Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrderSection.Client.ViewModel
{
    public class OrderModelIdexer
    {
        public OrderEntity OrderEntity { get; set; }

        public IEnumerable<ProductEntity> ProductEntityCollection { get; set; }
        public IEnumerable<CategoryEntity> CategoryEntityCollection { get; set; }
        public IEnumerable<OrderEntity> OrderEntityCollection { get; set; }
        public IEnumerable<Order_ItemsEntity> Order_ItemsEntityCollection { get; set; }
        public IEnumerable<StatusEntity> StatusEntityCollection { get; set; }
    }
}